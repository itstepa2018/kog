package kog.by.step.it

import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    val kingdom = Kingdom(ConsoleInOut)
    kingdom.start()

}

class Kingdom(io: InOut) {
    val warehouse = Warehouse()
    val population = Population()

    val land = Land()
    var years: Int = 0


    fun start() {
        while (years < 100) {
            println("Year: ${years + 1}")
            println("${warehouse.grain} grain")
            println("${population.people} people")
            println("${land.size} land")
            val inputGrain = inputEat(population = population, warehouse = warehouse)
            println("${warehouse.grain} grain")
            val inputLand = seedLand(warehouse, population, land)
            println("${land.size} land")
            when {
                inputLand <= land.size / 3 -> warehouse + inputLand * 20
                inputLand < land.size * 2 / 3 -> warehouse + inputLand * 10
                else -> warehouse + inputLand * 5
            }
            if (inputGrain <= 5) {
                population.people *= .9.toInt()
            }
            if(inputGrain>10)
            {population.people*=1.2.toInt()
            }
            println("${population.people} people")
            years++
        }
    }
}

fun inputEat(warehouse: Warehouse, population: Population): Int {
    while (true) {
        val inputGrain = ConsoleInOut.inputInt("How much grain per people")
        val amount = population.people * inputGrain
        try {
            warehouse - amount
            return inputGrain
        } catch (e: OutOfRangeException) {
            println("You need more grain. Try again")
        }
    }
}

fun seedLand(warehouse: Warehouse, population: Population, land: Land): Int {
    while (true) {
        val inputLand = ConsoleInOut.inputInt("How much land we prepare?")
        if (population.people >= inputLand && warehouse.grain >= inputLand * 2) {
            warehouse - inputLand * 2
            return inputLand
        }
    }
}

class Warehouse {
    var grain: Int
    val size: Int by lazy { 1000 }

    constructor() {
        println("Constructor")
        grain = Random().nextInt(500) + 500
    }

    operator fun minus(x: Int) {
        changeGrain(-x)
    }

    operator fun plus(x: Int) {
        changeGrain(x)
    }

    fun changeGrain(value: Int) {
        grain = if (grain + value in 0..size) min(grain + value, size)
        else throw OutOfRangeException("Warehouse size exception")
    }
}

class OutOfRangeException(s: String) : Exception(s) {

}

class Population() {
    var people = 10

    operator fun plus(x: Int) {
        people += x
    }

    operator fun Population.plus(x: Int) {
        people += x
    }
}

class Land() {
    var size = 50
    operator fun plus(x: Int) {
        size += x
    }

    operator fun minus(x: Int) {
        size -= x
    }
}


