package kog.by.step.it

interface InOut {
    fun input():String

    fun output(s:String)

    fun inputInt(s:String):Int
}