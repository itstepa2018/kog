package kog.by.step.it.menu

fun main(args: Array<String>) {
    val l = listOf("dima", "andrew",
            "nastya", "artem", "matvey", "artem")
    val t = listOf("danilov", "bondarenko", "posivets",
            "mihailov", "volochko", "petrovski")
    val i = l.iterator()

    val fName = l.map { it.capitalize() }
    val lName = t.map(String::capitalize)
    val fullName = fName.zip(lName) { a, b -> "$a $b" }
    val sorted = fullName.sortedBy { it.substring(it.indexOf(" ")) }
    val filtered = sorted.filter { it.contains("v") }
//    println(filtered.firstOrNull() ?: "No such elements")
//    filtered.take(2).forEach { println(it) }
//    sorted.takeWhile { it.length > 13 }.forEach { println(it) }
//    println(sorted.reduce { acc, s -> acc + s.substring(0, s.indexOf(" ")) })
//    print(sorted.fold("",            { acc, s -> acc + s.substring(0, s.indexOf(" ")) }))

//    sorted.distinctBy { s -> s.substring(0, s.indexOf(" ")) }.forEach { println(it) }
    sorted.flatMap { s ->
        listOf(s.substring(0, s.indexOf(" ")),
                s.substring(s.indexOf(" ")))
    }.map { it.replace(" ", "") }.forEach { println(it) }

}