package kog.by.step.it

import java.util.*

object ConsoleInOut : InOut{
    override fun input(): String = readLine()?: throw InputMismatchException()

    override fun output(s: String) = println(s)

    override fun inputInt(s: String) : Int {
        output(s)
        while (true){
            try {
               return  (readLine() ?: "").toInt()
            }catch (e:NumberFormatException){
                output("Incorrect number")
            }

        }

    }

}